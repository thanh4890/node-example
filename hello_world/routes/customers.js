var express = require('express');
var router = express.Router();

/* GET customers listing. */
router.get('/', function(req, res, next) {
  req.getConnection(function(err, connection) {
    connection.query('SELECT * FROM customer', function (err, rows) {
      if (err) {
        console.log('Error selecting: %s', err);
      }

      res.render('customers', {page_title:"Customers - Node.js", data: rows});
    });
  });
});

router.get('/add', function(req, res, next) {
  var data = [{
    name: '',
    address: '',
    email: '',
    phone: ''
  }]
  res.render('addCustomer', {page_title:"Add Customer - Node.js", data: data});
});

router.post('/add', function(req, res, next) {
  var input = JSON.parse(JSON.stringify(req.body));

  req.getConnection(function(err, connection) {
    var data = {
      name:    input.name,
      address: input.address,
      email:   input.email,
      phone:   input.phone
    };

    var query = connection.query('INSERT INTO customer set ?', data, function(err, rows) {
      if (err) {
        console.log('Error inserting: %s', err);
      }

      res.redirect('/customers');
    });

    // console.log(query.sql); // get raw query
  });
});

router.get('/edit/:id', function(req, res, next) {
  var id = req.params.id

  req.getConnection(function(err, connection) {
    connection.query('SELECT * FROM customer WHERE id = ?', [id], function(err, rows) {
      if (err) {
        console.log('Error selecting: %s', err);
      }

      res.render('editCustomer', {page_title: 'Edit Customer - Node.js', data: rows});
    });
  });
});

router.post('/edit/:id', function (req, res, next) {
  var input = JSON.parse(JSON.stringify(req.body));
  var id = req.params.id

  req.getConnection(function(err, connection) {
    var data = {
      name:    input.name,
      address: input.address,
      email:   input.email,
      phone:   input.phone
    };

    var query = connection.query('UPDATE customer set ? WHERE id = ?', [data, id], function(err, rows) {
      if (err) {
        console.log('Error updating: %s', err);
      }

      res.redirect('/customers')
    });

    console.log(query.sql);
  });
});

router.get('/delete/:id', function (req, res, next) {
  var id = req.params.id;

  req.getConnection(function(err, connection) {
    connection.query('DELETE FROM customer WHERE id = ?', [id], function(err, rows) {
      if (err) {
        console.log('Error deleting: %s', err);
      }

      res.redirect('/customers')
    });
  });
});

module.exports = router;
